package tk.theakio.sql;

public class InvalidDatabaseTypeException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidDatabaseTypeException() {}
	
	public InvalidDatabaseTypeException(String message) {
		super(message);
	}
	
}
