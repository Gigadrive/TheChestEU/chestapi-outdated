package eu.thechest.listener;

import java.util.ArrayList;
import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.help.HelpTopic;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;
import de.inventivegames.nickname.Nicks;
import de.inventivegames.rpapi.ResourcePackStatusEvent;
import eu.thechest.api.APIMeta;
import eu.thechest.api.BountifulAPI;
import eu.thechest.api.ChestAPI;
import eu.thechest.api.cmd.MainExecutor;
import eu.thechest.api.lang.LangVariable;
import eu.thechest.api.server.GameState;
import eu.thechest.api.server.GameStateManager;
import eu.thechest.api.user.ChestUser;
import eu.thechest.api.user.OfflinePlayerUtil;
import eu.thechest.api.user.Rank;
import eu.thechest.api.user.UUIDFetcher;
import eu.thechest.api.user.UserStorage;

public class MainListener implements Listener {

	public MainListener(){
		
	}

	private void remove(ArrayList<Player> a, Player all){
		a.remove(all);
	}
	
	@EventHandler
	public void onTabComplete(PlayerChatTabCompleteEvent e){
		Player p = e.getPlayer();
		
		if(UserStorage.getUserByPlayer(p).hasPermission(Rank.ADMIN) == false){
			e.getTabCompletions().clear();
		}
	}
	
	@EventHandler
    public void onResourcePackStatus(ResourcePackStatusEvent e) {
		Player p = e.getPlayer();
		ChestUser u = UserStorage.getUserByPlayer(p);
		
		if(u.hasPermission(Rank.ADMIN)){
			p.sendMessage("�6----------------------------------------------------");
			p.sendMessage("�bDetected Resource Pack Status Change! �e(�a" + e.getStatus().toString() + ":H_" + e.getHash() + "�e)");
			p.sendMessage("�6----------------------------------------------------");
		}
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent e){
		Player p = e.getPlayer();
		
		/*if(!e.getAddress().getHostAddress().equals("89.144.6.221")){
			e.setResult(Result.KICK_OTHER);
			e.setKickMessage("�cPlease join through �eplay.thechest.eu");
		}*/

		if(e.getResult() == PlayerLoginEvent.Result.KICK_FULL){
			if(OfflinePlayerUtil.hasPermission(p.getName(), Rank.PRO)){
				ArrayList<Player> players = new ArrayList<Player>();
				for(Player all : Bukkit.getOnlinePlayers()){
					if(!OfflinePlayerUtil.hasPermission(all.getName(), Rank.PRO)){
						players.add(all);
					}
				}

				if(players.size() == 0){
					e.setKickMessage("�cEs konnte kein geeigneter Platz gefunden werden.");
				} else {
					Collections.shuffle(players);

					if(UserStorage.getUserByPlayer(players.get(0)).getCurrentServerName().equals("LOBBY")){
						players.get(0).kickPlayer(APIMeta.CHAT_PREFIX_ERROR + UserStorage.getUserByPlayer(players.get(0)).getTranslatedMessage("join.kickedForPremium", null));
					} else {
						players.get(0).sendMessage(APIMeta.CHAT_PREFIX_ERROR + UserStorage.getUserByPlayer(players.get(0)).getTranslatedMessage("join.kickedForPremium", null));
						UserStorage.getUserByPlayer(players.get(0)).connectToLobby();
					}
					
					e.allow();
				}
			} else {
				e.setKickMessage("�cDer Server ist voll. �6Kaufe dir Premium auf �ehttp://thechest.eu/store");
			}
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		e.setJoinMessage(null);
		
		if(Nicks.isNicked(p.getUniqueId())){
			Nicks.removeNick(p.getUniqueId());
			Nicks.removeSkin(p.getUniqueId());
		}
		
		try {
			QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + UUIDFetcher.getUUID(p.getName()) + "'");
			qr.rs.last();
			
			if(qr.rs.getRow() == 0){
				HandlerStorage.getHandler("main").execute("INSERT INTO `users` (`uuid` ,`name`, `friends`)VALUES ('" + UUIDFetcher.getUUID(p.getName()) + "', '" + p.getName() + "', '0,');");
				UserStorage.register(p);
			}
		} catch(Exception e1){
			e1.printStackTrace();
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
			public void run(){
				ChestUser u = null;
				
				if(UserStorage.STORAGE.containsKey(p)){
					UserStorage.STORAGE.remove(p);
				}
				
				UserStorage.register(p);
				u = UserStorage.getUserByPlayer(p);
				
				u.updateDisplayName();
				u.achieve(1);
				BountifulAPI.sendTabTitle(p, "�6[TheChestEU]\n�aServer �e" + u.getCurrentServerName() + "\n", "\n�aTeamSpeak: �ets.thechest.eu\n�aWebsite: �ewww.thechest.eu");
				
				if(!u.hasPermission(Rank.STAFF)){
					for(String s : MainExecutor.VANISHED_PLAYERS){
						if(Bukkit.getPlayer(s) != null){
							p.hidePlayer(Bukkit.getPlayer(s));
						}
					}
				}
				
				if(u.hasPermission(Rank.BUILD_TEAM)){
					p.setOp(true);
				} else {
					p.setOp(false);
				}
				
				if(u.getAutoNick() && u.hasPermission(Rank.VIP)){
					p.performCommand("nick random");
				}
				
				if(u.getCurrentServerName().equalsIgnoreCase("EVENT")){
					GameStateManager.setGameState(GameState.JOINABLE);
				}
			}
		}, 10L);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		ChestUser u = UserStorage.getUserByPlayer(p);
		e.setQuitMessage(null);
		u.saveData();
		
		p.removeAttachment(u.getPermissionAttachment());
		
		if(MainExecutor.VANISHED_PLAYERS.contains(p.getName())){
			MainExecutor.VANISHED_PLAYERS.remove(p.getName());
		}
	}
	
	@EventHandler
	public void onChange(SignChangeEvent e){
		Player p = e.getPlayer();
		
		int i = 0;
		for(String s : e.getLines()){
			String line = e.getLine(i);
			line = line.replace("&0", "�0"); // BLACK
			line = line.replace("&1", "�1"); // VERY DARK BLUE
			line = line.replace("&2", "�2"); // DARK GREEN
			line = line.replace("&3", "�3"); // DARK AQUA
			line = line.replace("&4", "�4"); // DARK RED
			line = line.replace("&5", "�5"); // PURPLE
			line = line.replace("&6", "�6"); // GOLD
			line = line.replace("&7", "�7"); // LIGHT GREY
			line = line.replace("&8", "�8"); // DARK GREY
			line = line.replace("&9", "�9"); // BLUE
			
			line = line.replace("&a", "�a"); // LIGHT GREEN
			line = line.replace("&b", "�b"); // AQUA
			line = line.replace("&c", "�c"); // RED
			line = line.replace("&d", "�d"); // PINK
			line = line.replace("&e", "�e"); // YELLOW
			line = line.replace("&f", "�f"); // WHITE
			
			line = line.replace("&k", "�k"); // OBFUSCATED
			line = line.replace("&l", "�l"); // BOLD
			line = line.replace("&m", "�m"); // STRIKETHROUGH
			line = line.replace("&n", "�n"); // UNDERLINE
			line = line.replace("&o", "�o"); // ITALIC
			line = line.replace("&r", "�r"); // RESET
			
			e.setLine(i, line);
			i++;
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPreProcess(PlayerCommandPreprocessEvent e){
		Player p = e.getPlayer();
		
		if(!e.isCancelled()){
			String command = e.getMessage().split(" ")[0];
			HelpTopic htopic = Bukkit.getServer().getHelpMap().getHelpTopic(command);
			if(htopic == null){
				p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + UserStorage.getUserByPlayer(p).getTranslatedMessage("commandNotFound", null));
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		Player p = e.getEntity();
		
		if(p.getKiller() != null){
			Player killer = p.getKiller();
			
			if(UserStorage.getUserByPlayer(killer).hasAchieved(29)){
				if(!UserStorage.getUserByPlayer(p).hasAchieved(29)){
					UserStorage.getUserByPlayer(p).achieve(29);
					killer.sendMessage("�b[TheVirus] �6" + UserStorage.getUserByPlayer(killer).getTranslatedMessage("swarm.infectedSomeone", new LangVariable[]{new LangVariable("%who%", p.getDisplayName())}));
				}
			}
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		ChestUser u = UserStorage.getUserByPlayer(p);
		
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
			if(p.getItemInHand() != null && p.getItemInHand().getType() != null && p.getItemInHand().getItemMeta() != null && p.getItemInHand().getItemMeta().getDisplayName() != null){
				if(u.hasPermission(Rank.ADMIN)){
					if(p.getItemInHand().getItemMeta().getDisplayName() == "�cBlockLoc"){
						p.sendMessage("WORLD: " + e.getClickedBlock().getLocation().getWorld().getName());
						p.sendMessage("X: " + e.getClickedBlock().getLocation().getX());
						p.sendMessage("Y: " + e.getClickedBlock().getLocation().getY());
						p.sendMessage("Z: " + e.getClickedBlock().getLocation().getZ());
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onChat(PlayerChatEvent e){
		Player p = e.getPlayer();
		ChestUser u = UserStorage.getUserByPlayer(p);
		String msg = e.getMessage();
		
		if(msg.contains("%")){
			e.setCancelled(true);
		}
		
		if(msg.equalsIgnoreCase("gg")){
			u.achieve(27);
		}
		
		if(Nicks.isNicked(p.getUniqueId())){
			e.setFormat(Rank.USER.getColor() + Nicks.getNick(p.getUniqueId()) + "�8> �f" + e.getMessage());
		} else {
			if(u.getRank().mayDisplayPrefix()){
				e.setFormat(u.getRank().getColor() + "[" + u.getRank().getPrefix().toUpperCase() + "] " + p.getName() + "�8> �f" + e.getMessage());
			} else {
				e.setFormat(u.getRank().getColor() + p.getName() + "�8> �f" + e.getMessage());
			}
		}
		
		/*if(u.getRank().getDisplayPrefix()){
			e.setFormat("�8<[�7" + u.getRank().getName() + "�8] " + u.getRank().getColor() + p.getName() + "�8> �f" + e.getMessage());
		} else {
			e.setFormat("�8<" + u.getRank().getColor() + p.getName() + "�8> �f" + e.getMessage());
		}*/
	}
	
	@EventHandler
	public void onPing(ServerListPingEvent e){
		e.setMotd(GameStateManager.getGameState().getDisplay());
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	  public void onDestroyByEntity(HangingBreakByEntityEvent event)
	  {
	    if(ChestAPI.PROTECT_ITEM_FRAMES){
	    	if ((event.getRemover() instanceof Player))
		    {
		      Player p = (Player)event.getRemover();
		      if ((event.getEntity().getType() == EntityType.ITEM_FRAME) && 
		        (!p.isOp()) && (!p.hasPermission("frame.remove")))
		      {
		        event.setCancelled(true);
		        return;
		      }
		    }
	    }
	  }
	  
	  @EventHandler(priority=EventPriority.HIGHEST)
	  public void OnPlaceByEntity(HangingPlaceEvent event)
	  {
	    if(ChestAPI.PROTECT_ITEM_FRAMES){
	    	Player p = event.getPlayer();
		    if ((event.getEntity().getType() == EntityType.ITEM_FRAME) && 
		      (!p.isOp()) && (!p.hasPermission("frame.place")))
		    {
		      event.setCancelled(true);
		      return;
		    }
	    }
	  }
	  
	  @EventHandler
	  public void canRotate(PlayerInteractEntityEvent event)
	  {
	    if(ChestAPI.PROTECT_ITEM_FRAMES){
	    	Entity entity = event.getRightClicked();
		    Player player = event.getPlayer();
		    if (!entity.getType().equals(EntityType.ITEM_FRAME)) {
		      return;
		    }
		    ItemFrame iFrame = (ItemFrame)entity;
		    if ((iFrame.getItem().equals(null)) || (iFrame.getItem().getType().equals(Material.AIR))) {
		      return;
		    }
		    if ((!player.isOp()) && (!player.hasPermission("frame.rotate")))
		    {
		      event.setCancelled(true);
		    }
	    }
	  }
	  
	  @EventHandler
	  public void ItemRemoval(EntityDamageByEntityEvent e)
	  {
	    if(ChestAPI.PROTECT_ITEM_FRAMES){
	    	if ((e.getDamager() instanceof Player))
		    {
		      Player p = (Player)e.getDamager();
		      if ((e.getEntity().getType() == EntityType.ITEM_FRAME) && 
		        (!p.isOp()) && (!p.hasPermission("frame.item.remove")))
		      {
		        e.setCancelled(true);
		      }
		    }
		    if (((e.getDamager() instanceof Projectile)) && (e.getEntity().getType() == EntityType.ITEM_FRAME))
		    {
		      Projectile p = (Projectile)e.getDamager();
		      Player player = (Player)p.getShooter();
		      if ((!player.isOp()) && (!player.hasPermission("frame.item.remove")))
		      {
		        e.setCancelled(true);
		      }
		    }
	    }
	  }
	
}
