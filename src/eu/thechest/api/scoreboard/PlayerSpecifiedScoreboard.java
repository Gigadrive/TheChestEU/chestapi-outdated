package eu.thechest.api.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import eu.thechest.api.user.ChestUser;
import eu.thechest.api.user.UserStorage;

public class PlayerSpecifiedScoreboard {

	private Scoreboard sb;
	public PlayerSpecifiedScoreboard(ScoreboardType sbtype, Player p){
		ChestUser u = UserStorage.getUserByPlayer(p);
		sb = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective obj = sb.registerNewObjective("side", "dummy");
		obj.setDisplayName("�8[= �eplay.�6TheChest�e.eu �8=]");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		if(sbtype == ScoreboardType.LOBBY){
			obj.getScore(Bukkit.getOfflinePlayer("�6Coins:")).setScore(u.getCoins());
			obj.getScore(Bukkit.getOfflinePlayer("�3Chips:")).setScore(u.getChips());
			//obj.getScore(Bukkit.getOfflinePlayer("�5Coupons:")).setScore(u.getCoupons());
		}
		
		p.setScoreboard(sb);
	}
	
	public Scoreboard getScoreboard(){
		return this.sb;
	}
	
}
