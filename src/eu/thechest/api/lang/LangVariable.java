package eu.thechest.api.lang;

public class LangVariable {

	private String id;
	private String value;
	
	public LangVariable(String variableID, String variableValue){
		this.id = variableID;
		this.value = variableValue;
	}
	
	public String getVariableId(){
		return id;
	}
	
	public String getVariableValue(){
		return value;
	}
	
}
