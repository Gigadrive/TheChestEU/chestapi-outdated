package eu.thechest.api.lang;

import java.util.HashMap;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;

public class Translation {

	private HashMap<String, String> PHRASES;
	private String transCode;
	
	public Translation(String transCode){
		this.transCode = transCode;
		
		if(!LangUtil.STORAGE.containsKey(transCode)){
			PHRASES = new HashMap<String, String>();
			
			try {
				QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM phrases");
				qr.rs.first();
				
				while(qr.rs.next()){
					if(qr.rs.getString(transCode) != null && qr.rs.getString(transCode) != ""){
						PHRASES.put(qr.rs.getString("id"), qr.rs.getString(transCode));
					}
				}
				
				HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
			} catch(Exception e){
				e.printStackTrace();
			}
			
			LangUtil.STORAGE.put(transCode, this);
		}
	}
	
	public String getTranslation(String phraseID){
		if(PHRASES.containsKey(phraseID)){
			return PHRASES.get(phraseID);
		} else {
			return phraseID;
		}
	}
	
	public String getLanguageCode(){
		return this.transCode;
	}
	
	public HashMap<String, String> getPhrases(){
		return PHRASES;
	}
	
}
