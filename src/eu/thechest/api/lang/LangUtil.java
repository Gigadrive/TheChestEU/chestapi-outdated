package eu.thechest.api.lang;

import java.util.HashMap;

public class LangUtil {

	public static HashMap<String, Translation> STORAGE = new HashMap<String, Translation>();
	
	public static Translation getTranslations(String translationID){
		if(STORAGE.containsKey(translationID)){
			return STORAGE.get(translationID);
		} else {
			return null;
		}
	}
	
}
