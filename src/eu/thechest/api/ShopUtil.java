package eu.thechest.api;

import org.bukkit.entity.Player;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;
import eu.thechest.api.user.UserStorage;

public class ShopUtil {

	public static String getBuyers(int id) throws Exception {
		String buyers = "";
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM shopItems WHERE id=" + id + "");
		qr.rs.last();
		if(qr.rs.getRow() != 0){
			buyers = qr.rs.getString("bought_by");
		}
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return buyers;
	}
	
	public static boolean hasBoughtShopItem(Player p, int id) throws Exception {
		boolean b = false;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM shopItems WHERE id=" + id + "");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			if(qr.rs.getString("bought_by").contains("," + UserStorage.getUserByPlayer(p).getInternalId())){
				b = true;
			} else {
				b = false;
			}
		}
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return b;
	}
	
	public static int getCoinsForItem(int id) throws Exception {
		int b = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM shopItems WHERE id=" + id + "");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			b = qr.rs.getInt("price_coins");
		}
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return b;
	}
	
	public static int getChipsForItem(int id) throws Exception {
		int b = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM shopItems WHERE id=" + id + "");
		qr.rs.last();
		
		if(qr.rs.getRow() != 0){
			b = qr.rs.getInt("price_chips");
		}
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return b;
	}
	
	public static void buyShopItem(Player p, int id) throws Exception {
		HandlerStorage.getHandler("main").execute("UPDATE `shopItems` SET `bought_by` = '" + getBuyers(id) + UserStorage.getUserByPlayer(p).getInternalId() + ",' WHERE id=" + id + ";");
	}
	
}
