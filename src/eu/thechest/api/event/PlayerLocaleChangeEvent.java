package eu.thechest.api.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import eu.thechest.api.user.ChestUser;

public class PlayerLocaleChangeEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	private ChestUser user;
	private String oldLocale;
	private String newLocale;
	
	public PlayerLocaleChangeEvent(ChestUser user, String oldLocale, String newLocale){
		this.user = user;
		this.oldLocale = oldLocale;
		this.newLocale = newLocale;
	}
	
	public ChestUser getUser(){
		return user;
	}
	
	public String getOldLocale(){
		return oldLocale;
	}
	
	public String getNewLocale(){
		return newLocale;
	}
	
	public HandlerList getHandlers(){
		return handlers;
	}
	
	public static HandlerList getHandlerList(){
		return handlers;
	}
	
}
