package eu.thechest.api;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.PlayerInfoData;
import net.minecraft.server.v1_8_R3.PlayerConnection;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.util.CraftChatMessage;
import org.bukkit.entity.Player;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;

import com.mojang.authlib.GameProfile;

import eu.thechest.api.user.AlternateUUIDFetcher;
import eu.thechest.api.user.GameProfileBuilder;
import eu.thechest.api.user.Rank;
import eu.thechest.api.user.UserStorage;

public class NickUtil {
	
	public static ArrayList<String> RANDOM_NICKS = new ArrayList<String>();
	public static ArrayList<String> NICK_DELAY_STORAGE = new ArrayList<String>();
	public static HashMap<Player, String> NICK_STORAGE = new HashMap<Player, String>();
	
	public static void registerRandomNicks(){
		RANDOM_NICKS.clear();
		
		/*RANDOM_NICKS.add("IhateUngespielt"); // Mehdi war hier :P PS: war klar, dass ich hier war, ich hab das plugin geschrieben lol (07.05.2015 19:40)
		RANDOM_NICKS.add("tonne100");
		RANDOM_NICKS.add("mcreloader");
		RANDOM_NICKS.add("gigabythe");
		RANDOM_NICKS.add("grumpyCrafter");
		RANDOM_NICKS.add("ultron9");
		RANDOM_NICKS.add("Lambo23");
		RANDOM_NICKS.add("Hektorius");
		RANDOM_NICKS.add("Larax3");
		RANDOM_NICKS.add("Nyph");
		RANDOM_NICKS.add("Ork15000");
		RANDOM_NICKS.add("Indigor");
		RANDOM_NICKS.add("NexusBasher");
		RANDOM_NICKS.add("LukesFather");
		RANDOM_NICKS.add("Outscope22");
		RANDOM_NICKS.add("KaiserKeks");
		RANDOM_NICKS.add("HansImPech");
		RANDOM_NICKS.add("Brotcrafter1000");
		RANDOM_NICKS.add("Luli");
		RANDOM_NICKS.add("Keepah");
		RANDOM_NICKS.add("Geier96");
		RANDOM_NICKS.add("LeftRightLP");
		RANDOM_NICKS.add("Koshu");
		RANDOM_NICKS.add("Schrottmann");
		RANDOM_NICKS.add("Landwirt9000");
		RANDOM_NICKS.add("Clementina");
		RANDOM_NICKS.add("bananananana");
		RANDOM_NICKS.add("itropanda");
		RANDOM_NICKS.add("fabolous");
		RANDOM_NICKS.add("Zironius");
		RANDOM_NICKS.add("Ampelmann");
		RANDOM_NICKS.add("Zebra99");
		RANDOM_NICKS.add("PnetCrafter");
		RANDOM_NICKS.add("HaftiAbi");
		RANDOM_NICKS.add("ImaTomato");
		RANDOM_NICKS.add("craftattack_");
		RANDOM_NICKS.add("PvP83");
		RANDOM_NICKS.add("FutureCop");
		RANDOM_NICKS.add("LavaEimer");
		RANDOM_NICKS.add("BelegterSemmel");
		RANDOM_NICKS.add("Obenohne");
		RANDOM_NICKS.add("MaximusSpaxus");
		RANDOM_NICKS.add("Laqqen");
		RANDOM_NICKS.add("bozz02");
		RANDOM_NICKS.add("counterIcrafter");
		RANDOM_NICKS.add("anno1503");
		RANDOM_NICKS.add("tischbein");
		RANDOM_NICKS.add("haxball");
		RANDOM_NICKS.add("hanniballPvP");
		RANDOM_NICKS.add("nofear74");
		RANDOM_NICKS.add("WasserH2O");
		RANDOM_NICKS.add("Gegenfrage");
		RANDOM_NICKS.add("AngelohMerte");
		RANDOM_NICKS.add("Siebeneinhalb");
		RANDOM_NICKS.add("antonspielt");
		RANDOM_NICKS.add("halbfertigLP");
		RANDOM_NICKS.add("dagehtnochwas");
		RANDOM_NICKS.add("comeatme99");
		RANDOM_NICKS.add("doctor_what");
		RANDOM_NICKS.add("sirius_white");
		RANDOM_NICKS.add("outtayoleague");
		RANDOM_NICKS.add("popstar7");
		RANDOM_NICKS.add("justapotato_");
		RANDOM_NICKS.add("Eintopf");
		RANDOM_NICKS.add("Pocknanontnas");
		RANDOM_NICKS.add("TunelClahsch");
		RANDOM_NICKS.add("Luhk_Plyzs");
		RANDOM_NICKS.add("Mr_Bananenkopf");
		RANDOM_NICKS.add("KOTcraftHD_");
		RANDOM_NICKS.add("Nahrutoo");
		RANDOM_NICKS.add("chasanep");
		RANDOM_NICKS.add("moreabaudt");
		RANDOM_NICKS.add("Tony_Shoooot");
		RANDOM_NICKS.add("Niplx");
		RANDOM_NICKS.add("Nova2015");
		RANDOM_NICKS.add("Kryplix");
		RANDOM_NICKS.add("LeFressy");
		RANDOM_NICKS.add("PimmelKatchu_xD");
		RANDOM_NICKS.add("CydiarLP");
		RANDOM_NICKS.add("Rekotside");
		RANDOM_NICKS.add("SchweineCode");
		RANDOM_NICKS.add("_HuansnHDLPXD_");
		RANDOM_NICKS.add("Better_to_Hacki");
		RANDOM_NICKS.add("GetFreak");
		RANDOM_NICKS.add("Tezlor");
		RANDOM_NICKS.add("Tocksick");
		RANDOM_NICKS.add("iTzSwaggigames");
		RANDOM_NICKS.add("Just_Swag");
		RANDOM_NICKS.add("Inzest69_1halb");
		RANDOM_NICKS.add("HerrBerkmanOHG");
		RANDOM_NICKS.add("WolliWinni");
		RANDOM_NICKS.add("BananenkopfLP");
		RANDOM_NICKS.add("X_iTzLPHDPVPxD_");
		RANDOM_NICKS.add("hot_rosi_8V");
		RANDOM_NICKS.add("K4CK3M4CH7C00L");
		RANDOM_NICKS.add("Tardissl");
		RANDOM_NICKS.add("62498");
		RANDOM_NICKS.add("7613593KLO");
		RANDOM_NICKS.add("LenaGames88");
		RANDOM_NICKS.add("BatBangerLP");
		RANDOM_NICKS.add("PAULISTGAAY");
		RANDOM_NICKS.add("SebiiiiHD");
		RANDOM_NICKS.add("Pamesany");
		RANDOM_NICKS.add("Lamborghini17");
		RANDOM_NICKS.add("Snack_Crack");
		RANDOM_NICKS.add("Qifjit");
		RANDOM_NICKS.add("Gigilh");
		RANDOM_NICKS.add("Lxiyot");
		RANDOM_NICKS.add("Rpaox");
		RANDOM_NICKS.add("Cjowrim");
		RANDOM_NICKS.add("Jowgajoa");
		RANDOM_NICKS.add("Yeuhuh");
		RANDOM_NICKS.add("Xexaca");
		RANDOM_NICKS.add("Losonc");
		RANDOM_NICKS.add("Jedj");
		RANDOM_NICKS.add("Grusoop");
		RANDOM_NICKS.add("Bebhuqe");
		RANDOM_NICKS.add("Sasus");
		RANDOM_NICKS.add("Xqawze");
		RANDOM_NICKS.add("Zeyeqock");
		RANDOM_NICKS.add("Vqiwatew");
		RANDOM_NICKS.add("Maekuhug");
		RANDOM_NICKS.add("Meheuc");
		RANDOM_NICKS.add("Pvubupea");
		RANDOM_NICKS.add("Yajio");
		RANDOM_NICKS.add("Wadodeho");
		RANDOM_NICKS.add("Solaqise");
		RANDOM_NICKS.add("Wuhujaic");
		RANDOM_NICKS.add("Mesimies");
		RANDOM_NICKS.add("Kuxujeg");
		RANDOM_NICKS.add("Nopoho");
		RANDOM_NICKS.add("Xtixtiku");
		RANDOM_NICKS.add("Deyode");
		RANDOM_NICKS.add("Puseok");
		RANDOM_NICKS.add("Rovacuqu");
		RANDOM_NICKS.add("5752742");
		RANDOM_NICKS.add("786752");
		RANDOM_NICKS.add("7867824124");
		RANDOM_NICKS.add("21572452699");
		RANDOM_NICKS.add("5638122");
		RANDOM_NICKS.add("iTzKevlllp");
		RANDOM_NICKS.add("KevPLayZHD_");
		RANDOM_NICKS.add("McStailah");
		RANDOM_NICKS.add("MzStylee");
		RANDOM_NICKS.add("Grabforfreee_");
		RANDOM_NICKS.add("Lickdown");
		RANDOM_NICKS.add("physicccyaal");
		RANDOM_NICKS.add("rawfischy");
		RANDOM_NICKS.add("xX_XxPlayZxX_Xx");
		RANDOM_NICKS.add("AntiHivespieler");
		RANDOM_NICKS.add("Herthsu26");*/
		
		try {
			QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE rank = 0");
			qr.rs.first();
			
			if(qr.rs.getRow() != 0){
				while(qr.rs.next()){
					RANDOM_NICKS.add(qr.rs.getString("name"));
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}

    private static ExecutorService pool = Executors.newCachedThreadPool();
    private static Field modifiers = getField(Field.class, "modifiers");
    private static Field playerUuidField = getField(PacketPlayOutNamedEntitySpawn.class, "b");
    private static Field actionField = getField(PacketPlayOutPlayerInfo.class, "a");
    private static Field dataField = getField(PacketPlayOutPlayerInfo.class, "b");
    private static Field nameField = getField(GameProfile.class, "name");
    public static void nick(final Player p, final String name) {
       pool.execute(new Runnable() {
          @Override
          public void run() {
             try {               
                GameProfile prof = GameProfileBuilder.fetch(AlternateUUIDFetcher.getUUID(ChatColor.stripColor(name)));
                nameField.set(prof, name);
                
                EntityPlayer entity = ((CraftPlayer) p).getHandle();
                
                PacketPlayOutEntityDestroy despawn = new PacketPlayOutEntityDestroy(entity.getId());
                
                PacketPlayOutPlayerInfo removeProfile = new PacketPlayOutPlayerInfo();
                setInfo(removeProfile, EnumPlayerInfoAction.REMOVE_PLAYER, removeProfile.new PlayerInfoData(entity.getProfile(), -1, null, null));
                
                PacketPlayOutPlayerInfo info = new PacketPlayOutPlayerInfo();
                setInfo(info, EnumPlayerInfoAction.ADD_PLAYER, info.new PlayerInfoData(prof, entity.ping, entity.playerInteractManager.getGameMode(), CraftChatMessage.fromString(name)[0]));
                
                PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(entity);
                playerUuidField.set(spawn, prof.getId());
                
                List<Player> players = (List<Player>) Bukkit.getOnlinePlayers();
                
                synchronized (players) {
                   for (Player player : players) {
                      if (UserStorage.getUserByPlayer(player).hasPermission(Rank.VIP)) continue;
                      
                      PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                      
                      connection.sendPacket(despawn);
                      connection.sendPacket(removeProfile);
                   }
                }
                
                synchronized (this) {
                   wait(50L);                  
                }
                
                synchronized (players) {
                   for (Player player : players) {
                      if (UserStorage.getUserByPlayer(player).hasPermission(Rank.VIP)) continue;
                      
                      PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                      
                      connection.sendPacket(info);
                      connection.sendPacket(spawn);
                   }
                }
             } catch (Exception e) {
                e.printStackTrace();
             }
          }
       });
    }
    private static PacketPlayOutPlayerInfo setInfo(PacketPlayOutPlayerInfo packet, EnumPlayerInfoAction action, PlayerInfoData... data) {
       try {
          actionField.set(packet, action);
          dataField.set(packet, Arrays.asList(data));
       } catch (Exception e) {
          e.printStackTrace();
       }
       return packet;
    }
    private static Field getField(Class<?> clazz, String name) {
       try {
          Field field = clazz.getDeclaredField(name);
          field.setAccessible(true);
          
          if (Modifier.isFinal(field.getModifiers())) {
             modifiers.set(field, field.getModifiers() & ~Modifier.FINAL);
          }
          
          return field;
       } catch (Exception e) {
          e.printStackTrace();
       }
       return null;
    }
	
}
