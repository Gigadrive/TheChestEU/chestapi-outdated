package eu.thechest.api;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scoreboard.Team;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.MySQLData;
import tk.theakio.sql.SQLHandler;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import eu.thechest.api.cmd.MainExecutor;
import eu.thechest.api.event.PlayerLocaleChangeEvent;
import eu.thechest.api.lang.LangUtil;
import eu.thechest.api.lang.LangVariable;
import eu.thechest.api.lang.Translation;
import eu.thechest.api.server.GameState;
import eu.thechest.api.server.GameStateManager;
import eu.thechest.api.user.ChestUser;
import eu.thechest.api.user.OfflinePlayerUtil;
import eu.thechest.api.user.Rank;
import eu.thechest.api.user.UUIDFetcher;
import eu.thechest.api.user.UserStorage;
import eu.thechest.listener.MainListener;

public class ChestAPI extends JavaPlugin implements PluginMessageListener {
	
	public static boolean running = true;
	public static int PID = 0;
	public static long interval = 300L;
	public static int currentLine = 0;

	private static ChestAPI instance;
	public static ChestAPI getInstance(){
		return instance;
	}
	
	public static Team OWNER;
	public static Team ADMIN;
	public static Team MOD;
	public static Team TEAM;
	public static Team BUILD;
	public static Team YT;
	public static Team VIP;
	public static Team PREMIUM;
	public static Team USER;
	public static Team STAFF_MEMBERS;
	
	public static HashMap<String, String[]> PROXY_PLAYER_LIST = new HashMap<String, String[]>();
	
	public static boolean ALLOW_NICK;
	public static boolean ALLOW_VIP_JOIN;
	public static boolean PROTECT_ITEM_FRAMES;
	public static boolean CHANGE_TABLIST_NAME;
	
	public void onLoad(){
		GameStateManager.setGameState(GameState.UNDEFINED);
	}
	
	public static void createNickLog(String who, String nick){
		try {
			HandlerStorage.getHandler("main").execute("INSERT INTO `nickLog` (`player`, `nick`)VALUES ('" + UUIDFetcher.getUUID(who) + "', '" + nick + "');");
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void onEnable(){
		ALLOW_NICK = true;
		ALLOW_VIP_JOIN = true;
		PROTECT_ITEM_FRAMES = true;
		CHANGE_TABLIST_NAME = true;
		
		SQLHandler Handler = HandlerStorage.addHandler("main", new SQLHandler());
		Handler.setMySQLOptions(MySQLData.mysql_u_host, MySQLData.mysql_u_port, MySQLData.mysql_u_user, MySQLData.mysql_u_pass, MySQLData.mysql_u_database);
		try {
			Handler.openConnection();
		} catch (Exception e) {
			System.err.println("[MySQL API by TheAkio] BROKEN!");
			e.printStackTrace();
		}
		
		MainExecutor exec = new MainExecutor();
		getCommand("rank").setExecutor(exec);
		getCommand("gamemode").setExecutor(exec);
		getCommand("skull").setExecutor(exec);
		getCommand("money").setExecutor(exec);
		getCommand("fly").setExecutor(exec);
		getCommand("debug").setExecutor(exec);
		getCommand("nick").setExecutor(exec);
		getCommand("nicklist").setExecutor(exec);
		getCommand("settings").setExecutor(exec);
		getCommand("blockloc").setExecutor(exec);
		getCommand("help").setExecutor(exec);
		getCommand("fixplayer").setExecutor(exec);
		getCommand("nick").setExecutor(exec);
		getCommand("redeemkey").setExecutor(exec);
		getCommand("vanish").setExecutor(exec);
		getCommand("clearchat").setExecutor(exec);
		getCommand("addchips").setExecutor(exec);
		getCommand("addcoupons").setExecutor(exec);
		getCommand("premium").setExecutor(exec);
		getCommand("insertpaymentdata").setExecutor(exec);
		getCommand("lifetimepremium").setExecutor(exec);
		getCommand("unpremium").setExecutor(exec);
		getCommand("setlocation").setExecutor(exec);
		getCommand("goto").setExecutor(exec);
		getCommand("reward").setExecutor(exec);
		
		Bukkit.getPluginManager().registerEvents(new MainListener(), this);
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
		
		instance = this;
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("OWNER") == null){
			OWNER = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("OWNER");
			OWNER.setDisplayName("OWNER");
			OWNER.setPrefix(Rank.OWNER.getColor().toString());
		} else {
			OWNER = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("OWNER");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("ADMIN") == null){
			ADMIN = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("ADMIN");
			ADMIN.setDisplayName("ADMIN");
			ADMIN.setPrefix(Rank.ADMIN.getColor().toString());
		} else {
			ADMIN = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("ADMIN");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("MOD") == null){
			MOD = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("MOD");
			MOD.setDisplayName("MOD");
			MOD.setPrefix(Rank.MOD.getColor().toString());
		} else {
			MOD = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("MOD");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("TEAM") == null){
			TEAM = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("TEAM");
			TEAM.setDisplayName("TEAM");
			TEAM.setPrefix(Rank.STAFF.getColor().toString());
		} else {
			TEAM = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("TEAM");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("BUILD") == null){
			BUILD = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("BUILD");
			BUILD.setDisplayName("BUILD");
			BUILD.setPrefix(Rank.BUILD_TEAM.getColor().toString());
		} else {
			BUILD = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("BUILD");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("VIP") == null){
			VIP = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("VIP");
			VIP.setDisplayName("VIP");
			VIP.setPrefix(Rank.VIP.getColor().toString());
		} else {
			VIP = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("VIP");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("YT") == null){
			YT = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("YT");
			YT.setDisplayName("YT");
			YT.setPrefix(Rank.VIP.getColor().toString());
		} else {
			YT = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("YT");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("PREMIUM") == null){
			PREMIUM = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("PREMIUM");
			PREMIUM.setDisplayName("PREMIUM");
			PREMIUM.setPrefix(Rank.PRO.getColor().toString());
		} else {
			PREMIUM = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("PREMIUM");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("USER") == null){
			USER = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("USER");
			USER.setDisplayName("USER");
			USER.setPrefix(Rank.USER.getColor().toString());
		} else {
			USER = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("USER");
		}
		
		if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam("STAFFMEMBERS") == null){
			STAFF_MEMBERS = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("STAFFMEMBERS");
			STAFF_MEMBERS.setCanSeeFriendlyInvisibles(true);
		} else {
			STAFF_MEMBERS = Bukkit.getScoreboardManager().getMainScoreboard().getTeam("STAFFMEMBERS");
			STAFF_MEMBERS.setCanSeeFriendlyInvisibles(true);
		}
		
		PID = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
		      public void run(){
		    	  ChestAPI.broadcastMessages();
		      }
		    }, 200L, interval * 20L);
		
		new Translation("DE");
		new Translation("EN");
		new Translation("DEN");
		
		NickUtil.registerRandomNicks();
		OfflinePlayerUtil.startCacheUpdater();
		
		try {
			OWNER.unregister();
			ADMIN.unregister();
			MOD.unregister();
			TEAM.unregister();
			BUILD.unregister();
			VIP.unregister();
			YT.unregister();
			PREMIUM.unregister();
			USER.unregister();
			STAFF_MEMBERS.unregister();
		} catch(Exception e){}
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			public void run(){
				for(Player all : Bukkit.getOnlinePlayers()){
					ChestUser u = UserStorage.getUserByPlayer(all);
					
					if(u.getStoredLocaleString() != u.convertCurrentLocaleString(all.getName())){
						PlayerLocaleChangeEvent event = new PlayerLocaleChangeEvent(u, u.getStoredLocaleString(), u.convertCurrentLocaleString(all.getName()));
						Bukkit.getServer().getPluginManager().callEvent(event);
						
						u.updateLocale();
						
						if(u.canReceiveLocaleChangedMessage()){
							all.sendMessage(APIMeta.CHAT_PREFIX + u.getTranslatedMessage("localeChanged", null));
						} else {
							u.updateCanReceiveLocaleChangedMessage();
						}
					}
				}
			}
		}, 1*20, 3*20);
	}
	
	public static void broadcastTranslatedMessage(String phraseID, LangVariable[] variables){
		for(Player all : Bukkit.getOnlinePlayers()){
			UserStorage.getUserByPlayer(all).sendTranslatedMessage(phraseID, variables);
		}
	}
	
	public static void broadcastTranslatedMessage(String phraseID, LangVariable[] variables, String prefix){
		for(Player all : Bukkit.getOnlinePlayers()){
			all.sendMessage(prefix + UserStorage.getUserByPlayer(all).getTranslatedMessage(phraseID, variables));
		}
	}
	
	@SuppressWarnings("resource")
	public static void broadcastMessages() {
	    ArrayList<String> LINES = new ArrayList<String>();
	    for(String s : LangUtil.getTranslations("DE").getPhrases().keySet()){
	    	if(s.startsWith("bc.line.")){
	    		LINES.add(s);
	    	}
	    }
	    
	    String line = LINES.get(currentLine);
	    
	    line = line.replaceAll("&a", "�a");
	    line = line.replaceAll("&c", "�c");
	    line = line.replaceAll("&4", "�4");
	    line = line.replaceAll("&6", "�6");
	    line = line.replaceAll("&l", "�l");
	    line = line.replaceAll("&o", "�o");
	    line = line.replaceAll("&9", "�9");
	    line = line.replaceAll("&0", "�0");
	    line = line.replaceAll("&8", "�8");
	    line = line.replaceAll("&9", "�9");
	    line = line.replaceAll("&d", "�d");
	    line = line.replaceAll("&e", "�e");
	    line = line.replaceAll("&3", "�3");
	    line = line.replaceAll("&b", "�b");
	    line = line.replaceAll("&7", "�7");
	    line = line.replaceAll("&1", "�1");
	    line = line.replaceAll("&2", "�2");
	    line = line.replaceAll("&5", "�5");
	    line = line.replaceAll("&f", "�f");
	    line = line.replaceAll("&r", "�r");
	 
	    broadcastTranslatedMessage(line, null, "�4[*] �r�7");
	    
	    int lastLine = LINES.size() - 1;
	    if (currentLine + 1 == lastLine + 1) {
	      currentLine = 0;
	    } else {
	      currentLine += 1;
	    }
	}
	
	public static void sendMapInfos(String name, String builder, String link){
		for(Player p : Bukkit.getOnlinePlayers()){
			p.sendMessage(APIMeta.CHAT_PREFIX + "�c---------------------------------");
			p.sendMessage(APIMeta.CHAT_PREFIX + "");
			p.sendMessage(APIMeta.CHAT_PREFIX + UserStorage.getUserByPlayer(p).getTranslatedMessage("mapVoting.nameTitle", null) + ": �6" + name);
			p.sendMessage(APIMeta.CHAT_PREFIX + UserStorage.getUserByPlayer(p).getTranslatedMessage("mapVoting.builderTitle", null) + ": �6" + builder);
			p.sendMessage(APIMeta.CHAT_PREFIX + "");
			p.sendMessage(APIMeta.CHAT_PREFIX + UserStorage.getUserByPlayer(p).getTranslatedMessage("mapVoting.haveFun", null));
			p.sendMessage(APIMeta.CHAT_PREFIX + "");
			p.sendMessage(APIMeta.CHAT_PREFIX + "�c---------------------------------");
		}
	}
	
	public static void sendMapInfos(Player p, String name, String builder, String link){
		p.sendMessage(APIMeta.CHAT_PREFIX + "�c---------------------------------");
		p.sendMessage(APIMeta.CHAT_PREFIX + "");
		p.sendMessage(APIMeta.CHAT_PREFIX + UserStorage.getUserByPlayer(p).getTranslatedMessage("mapVoting.nameTitle", null) + ": �e" + name);
		p.sendMessage(APIMeta.CHAT_PREFIX + UserStorage.getUserByPlayer(p).getTranslatedMessage("mapVoting.builderTitle", null) + ": �e" + builder);
		p.sendMessage(APIMeta.CHAT_PREFIX + "");
		p.sendMessage(APIMeta.CHAT_PREFIX + "�e" + UserStorage.getUserByPlayer(p).getTranslatedMessage("mapVoting.haveFun", null));
		p.sendMessage(APIMeta.CHAT_PREFIX + "");
		p.sendMessage(APIMeta.CHAT_PREFIX + "�c---------------------------------");
	}
	
	public void onDisable(){
		for(Player all : Bukkit.getOnlinePlayers()){
			UserStorage.getUserByPlayer(all).saveData();
		}
		
		try {
			OWNER.unregister();
			ADMIN.unregister();
			MOD.unregister();
			TEAM.unregister();
			BUILD.unregister();
			VIP.unregister();
			YT.unregister();
			PREMIUM.unregister();
			USER.unregister();
		} catch(Exception e){}
	}
	
	public static void requestPlayerList(String server){
		// server = ALL for global list
		
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("PlayerList");
		out.writeUTF(server);
		
		Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
		player.sendPluginMessage(ChestAPI.getInstance(), "BungeeCord", out.toByteArray());
	}
	
	public static void sendMessageProxy(String player, String msg){
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Message");
		out.writeUTF(player);
		out.writeUTF(msg);
		
		Player ayerPl = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
		ayerPl.sendPluginMessage(ChestAPI.getInstance(), "BungeeCord", out.toByteArray());
	}
	
	public static void broadcastMessageProxy(String msg){
		if(!PROXY_PLAYER_LIST.containsKey("ALL")){
			requestPlayerList("ALL");
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
			public void run(){
				for(String s : PROXY_PLAYER_LIST.get("ALL")){
					sendMessageProxy(s, msg);
				}
			}
		});
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if(channel.equals("BungeeCord")){
			ByteArrayDataInput in = ByteStreams.newDataInput(message);
		    String subchannel = in.readUTF();
		    
		    if(subchannel.equals("PlayerList")){
		    	String server = in.readUTF();
		    	String playerListString = in.readUTF();
		    	String[] playerList = playerListString.split(", ");
		    	
		    	if(PROXY_PLAYER_LIST.containsKey(server)){
		    		PROXY_PLAYER_LIST.remove(server);
		    	}
		    	
		    	PROXY_PLAYER_LIST.put(server, playerList);
		    	
		    	System.out.println("[PLMSG] Server " + server + " Playerlist: " + playerListString);
		    }
		}
	}
	
	public static Location getLocationFromConfig(String id){
		if(ChestAPI.getInstance().getConfig().get("location." + id + ".world") != null && ChestAPI.getInstance().getConfig().get("location." + id + ".x") != null && ChestAPI.getInstance().getConfig().get("location." + id + ".y") != null && ChestAPI.getInstance().getConfig().get("location." + id + ".z") != null && ChestAPI.getInstance().getConfig().get("location." + id + ".yaw") != null && ChestAPI.getInstance().getConfig().get("location." + id + ".pitch") != null){
			World w = Bukkit.getWorld(ChestAPI.getInstance().getConfig().getString("location." + id + ".world"));
			double x = ChestAPI.getInstance().getConfig().getDouble("location." + id + ".x");
			double y = ChestAPI.getInstance().getConfig().getDouble("location." + id + ".y");
			double z = ChestAPI.getInstance().getConfig().getDouble("location." + id + ".z");
			float yaw = ChestAPI.getInstance().getConfig().getInt("location." + id + ".yaw");
			float pitch = ChestAPI.getInstance().getConfig().getInt("location." + id + ".pitch");
			
			return new Location(w, x, y, z, yaw, pitch);
		} else {
			return null;
		}
	}
	
	public static void saveLocationToConfig(Location loc, String id){
		ChestAPI.getInstance().getConfig().set("location." + id + ".world", loc.getWorld().getName());
		ChestAPI.getInstance().getConfig().set("location." + id + ".x", loc.getX());
		ChestAPI.getInstance().getConfig().set("location." + id + ".y", loc.getY());
		ChestAPI.getInstance().getConfig().set("location." + id + ".z", loc.getZ());
		ChestAPI.getInstance().getConfig().set("location." + id + ".yaw", loc.getYaw());
		ChestAPI.getInstance().getConfig().set("location." + id + ".pitch", loc.getPitch());
		ChestAPI.getInstance().saveConfig();
	}
	
}
