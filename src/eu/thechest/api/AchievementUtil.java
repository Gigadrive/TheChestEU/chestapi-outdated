package eu.thechest.api;

import java.util.ArrayList;
import java.util.Arrays;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;

public class AchievementUtil {

	public static String getName(int aid) {
		try {
			String name = "";
			
			QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM achievements WHERE id='" + aid + "'");
			qr.rs.last();
			
			name = qr.rs.getString("name");
			
			HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
			
			return name;
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getDescription(int aid) {
		try {
			String description = "";
			
			QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM achievements WHERE id='" + aid + "'");
			qr.rs.last();
			
			description = qr.rs.getString("description");
			
			HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
			
			return description;
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static ArrayList<Integer> getAchievers(int aid) {
		try {
			String achieved_by = "";
			
			QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM achievements WHERE id='" + aid + "'");
			qr.rs.last();
			
			achieved_by = qr.rs.getString("achieved_by");
			
			HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
			
			ArrayList<Integer> a = new ArrayList<Integer>();
			for(String ss : achieved_by.split(",")){
				a.add(Integer.parseInt(ss));
			}
			
			return a;
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
}
