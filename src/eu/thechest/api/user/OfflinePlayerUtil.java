package eu.thechest.api.user;

import java.util.HashMap;

import org.bukkit.Bukkit;

import eu.thechest.api.ChestAPI;
import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;

public class OfflinePlayerUtil {
	
	private static HashMap<String, Rank> RANK_CACHE = new HashMap<String, Rank>();
	private static HashMap<Integer, String> ID_NAME_CACHE = new HashMap<Integer, String>();
	private static HashMap<String, String> UUID_NAME_CACHE = new HashMap<String, String>();
	
	public static void startCacheUpdater(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(ChestAPI.getInstance(), new Runnable(){
			public void run(){
				RANK_CACHE.clear();
			}
		}, 1*20, 10*20*60);
	}

	public static String getUsernameByInternalId(int id){
		if(ID_NAME_CACHE.containsKey(id)){
			return ID_NAME_CACHE.get(id);
		} else {
			try {
				QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE id=" + id);
				qr.rs.last();
				
				if(qr.rs.getRow() != 0){
					ID_NAME_CACHE.put(id, qr.rs.getString("name"));
					return qr.rs.getString("name");
				}
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public static int getInternalIdByUsername(String name){
		if(ID_NAME_CACHE.containsValue(name)){
			for(int i : ID_NAME_CACHE.keySet()){
				if(ID_NAME_CACHE.get(i).equals(name)){
					return i;
				}
			}
		} else {
			try {
				QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE name='" + name + "'");
				qr.rs.last();
				
				if(qr.rs.getRow() != 0){
					ID_NAME_CACHE.put(qr.rs.getInt("id"), name);
					return qr.rs.getInt("id");
				}
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return 0;
	}

	public static boolean hasPermission(String name, Rank minRank){
		if(getRank(name).getId() < minRank.getId()){
			return false;
		} else {
			return true;
		}
	}
	
	public static Rank getRank(String name){
		if(Bukkit.getPlayerExact(name) != null){
			return UserStorage.getUserByPlayer(Bukkit.getPlayerExact(name)).getRank();
		} else {
			if(RANK_CACHE.containsKey(name)){
				return RANK_CACHE.get(name);
			} else {
				try {
					QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE name='" + name + "'");
					qr.rs.last();
					
					if(qr.rs.getRow() != 0){
						Rank rank = Rank.fromId(qr.rs.getInt("rank"));
						RANK_CACHE.put(name, rank);
						return rank;
					}
				} catch(Exception e){
					e.printStackTrace();
				}
				
				return Rank.USER;
			}
		}
	}
	
	public static String getNameFromUUID(String uuid){
		if(UUID_NAME_CACHE.containsKey(uuid)){
			return UUID_NAME_CACHE.get(uuid);
		} else {
			try {
				QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + uuid + "'");
				qr.rs.last();

				if (qr.rs.getRow() != 0){
					return qr.rs.getString("name");
				}
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
}
