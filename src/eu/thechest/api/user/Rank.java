package eu.thechest.api.user;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.scoreboard.Team;

import eu.thechest.api.ChestAPI;

public enum Rank {

	OWNER(11, "Owner", "OWNER", true, ChestAPI.OWNER, ChatColor.DARK_RED),
	ADMIN(10, "Administrator", "ADMIN", true, ChestAPI.ADMIN, ChatColor.DARK_RED),
	COMMUNITY_MANAGER(9, "Community Manager", "CM", true, ChestAPI.ADMIN, ChatColor.RED),
	SENIOR_MOD(8, "Senior Moderator", "SRMOD", true, ChestAPI.MOD, ChatColor.DARK_GREEN),
	MOD(7, "Moderator", "MOD", true, ChestAPI.MOD, ChatColor.GREEN),
	STAFF(6, "Staff Member", "STAFF", true, ChestAPI.TEAM, ChatColor.YELLOW),
	BUILD_TEAM(5, "Building Team", "BUILDTEAM", true, ChestAPI.BUILD, ChatColor.BLUE),
	VIP(4, "VIP", "VIP", true, ChestAPI.VIP, ChatColor.DARK_PURPLE),
	TITAN(3, "Titan", "TITAN", true, ChestAPI.PREMIUM, ChatColor.AQUA),
	PRO_PLUS(2, "Pro+", "PRO+", true, ChestAPI.PREMIUM, ChatColor.DARK_AQUA),
	PRO(1, "Pro", "PRO", true, ChestAPI.PREMIUM, ChatColor.GOLD),
	USER(0, "User", null, false, ChestAPI.USER, ChatColor.DARK_GRAY);
	
	private int id;
	private String name;
	private String prefix;
	private boolean displayname;
	private Team sbteam;
	private ChatColor color;
	
	Rank(int id, String name, String prefix, boolean displayname, Team sbteam, ChatColor color){
		this.id = id;
		this.name = name;
		this.prefix = prefix;
		this.displayname = displayname;
		this.sbteam = sbteam;
		this.color = color;
	}
	
	public int getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getPrefix(){
		return this.prefix;
	}
	
	public boolean mayDisplayPrefix(){
		return this.displayname;
	}
	
	public Team getScoreboardTeam(){
		return this.sbteam;
	}
	
	public ChatColor getColor(){
		return this.color;
	}
	
	public static Rank fromId(int id){
		for(Rank r : values()){
			if(r.getId() == id){
				return r;
			}
		}
		
		return Rank.USER;
	}
	
}
