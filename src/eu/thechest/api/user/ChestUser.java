package eu.thechest.api.user;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutGameStateChange;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scoreboard.DisplaySlot;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;
import de.inventivegames.nickname.Nicks;
import eu.thechest.api.APIMeta;
import eu.thechest.api.AchievementUtil;
import eu.thechest.api.ChestAPI;
import eu.thechest.api.lang.LangUtil;
import eu.thechest.api.lang.LangVariable;
import eu.thechest.api.lang.Translation;
import eu.thechest.api.scoreboard.PlayerSpecifiedScoreboard;
import eu.thechest.api.scoreboard.ScoreboardType;

public class ChestUser {

	private int id;
	private Player p;
	private Rank rank;
	private int coins;
	private int chips;
	private int coupons;
	private int globalPoints;
	private String friendarray;
	private ArrayList<String> friends;
	private ArrayList<Integer> unlockedRewards;
	private boolean optionServerLocation;
	private boolean optionShowStats;
	private boolean optionAutoNick;
	private boolean optionFriendRequests;
	private boolean optionShowPlayers;
	private boolean optionKeepSpawn;
	private boolean optionPrivateMsg;
	private ScoreboardType sbtype;
	private PlayerSpecifiedScoreboard pssb;
	private String server;
	private String convertedLocale;
	private boolean canReceiveChangedMessage;
	private PermissionAttachment permAttachment;
	
	public ChestUser(Player p){
		try {
			QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM users WHERE uuid='" + UUIDFetcher.getUUID(p.getName()) + "'");
			qr.rs.last();

			if (qr.rs.getRow() != 0){
				this.id = qr.rs.getInt("id");
				this.rank = Rank.fromId(qr.rs.getInt("rank"));
				this.p = p;
				this.coins = qr.rs.getInt("coins");
				this.chips = qr.rs.getInt("chips");
				this.coupons = qr.rs.getInt("coupons");
				this.globalPoints = qr.rs.getInt("global_points");
				this.friendarray = qr.rs.getString("friends");
				this.optionServerLocation = APIMeta.convertIntegerToBoolean(qr.rs.getInt("setting_serverLocation"));
				this.optionShowStats = APIMeta.convertIntegerToBoolean(qr.rs.getInt("setting_showStats"));
				this.optionAutoNick = APIMeta.convertIntegerToBoolean(qr.rs.getInt("setting_autoNick"));
				this.optionFriendRequests = APIMeta.convertIntegerToBoolean(qr.rs.getInt("setting_allowFriendRequests"));
				this.optionShowPlayers = APIMeta.convertIntegerToBoolean(qr.rs.getInt("setting_showPlayers"));
				this.optionKeepSpawn = APIMeta.convertIntegerToBoolean(qr.rs.getInt("setting_keepSpawn"));
				this.optionPrivateMsg = APIMeta.convertIntegerToBoolean(qr.rs.getInt("setting_privateMsg"));
				this.sbtype = ScoreboardType.NONE;
				this.pssb = null;
				this.server = qr.rs.getString("server");
				this.convertedLocale = qr.rs.getString("locale");
				this.canReceiveChangedMessage = false;
				this.permAttachment = p.addAttachment(ChestAPI.getInstance());
				
				this.friends = new ArrayList<String>();
				this.unlockedRewards = new ArrayList<Integer>();
				for(String ss : friendarray.split(",")){
					if(OfflinePlayerUtil.getUsernameByInternalId(Integer.parseInt(ss)) != null && !this.friends.contains(OfflinePlayerUtil.getUsernameByInternalId(Integer.parseInt(ss)))){
						this.friends.add(OfflinePlayerUtil.getUsernameByInternalId(Integer.parseInt(ss)));
					}
				}
				
				syncRewards();
		    }
			
			HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		} catch(Exception e1){
			e1.printStackTrace();
		}
	}
	
	public int getInternalId(){
		return this.id;
	}
	
	public void syncRewards(){
		unlockedRewards.clear();
		
		try {
			QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM rewards");
			qr.rs.beforeFirst();
			
			while(qr.rs.next()){
				ArrayList<Integer> a = new ArrayList<Integer>();
				
				for(String s : qr.rs.getString("who_gained").split(",")){
					if(!s.equals("0")){
						a.add(Integer.parseInt(s));
					}
				}
				
				if(a.contains(getInternalId())){
					unlockedRewards.add(qr.rs.getInt("id"));
				}
			}
			
			HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public ArrayList<Integer> getUnlockedRewards(){
		return this.unlockedRewards;
	}
	
	public int getEmptySlotsInInventory(){
		int count = 0;
		for (ItemStack i : p.getInventory()) {
			if (i == null) {
				count++;
			}
		}
		
		return count;
	}
	
	public Player getBukkitPlayer(){
		return this.p;
	}
	
	public Rank getRank(){
		return this.rank;
	}
	
	public void setRank(Rank rank){
		this.rank = rank;
	}
	
	public int getCoins(){
		return this.coins;
	}
	
	public void setCoins(int coins){
		this.coins = coins;
		
		if(this.coins < 0){
			this.coins = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
	}
	
	public void addCoins(int coins){
		this.coins = this.coins + coins;
		
		if(this.coins < 0){
			this.coins = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
		
		p.sendMessage(APIMeta.CHAT_PREFIX + getTranslatedMessage("coins.gained", new LangVariable[]{new LangVariable("%gainedCoins%", coins + ""), new LangVariable("%newCoins%", getCoins() + "")}));
	}
	
	public void reduceCoins(int coins){
		this.coins = this.coins - coins;
		
		if(this.coins < 0){
			this.coins = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
	}
	
	public void addGlobalPoints(int points){
		this.globalPoints = this.globalPoints + points;
	}
	
	public int getGlobalPoints(){
		return this.globalPoints;
	}
	
	public int getChips(){
		return this.chips;
	}
	
	public PermissionAttachment getPermissionAttachment(){
		return this.permAttachment;
	}
	
	public void giveBukkitPermission(String permission){
		this.permAttachment.setPermission(permission, true);
	}
	
	public String convertCurrentLocaleString(String playerName){
		return getTranslation().getLanguageCode();
	}
	
	public String getStoredLocaleString(){
		return convertedLocale;
	}
	
	public void updateLocale(){
		convertedLocale = convertCurrentLocaleString(getBukkitPlayer().getName());
	}
	
	public boolean canReceiveLocaleChangedMessage(){
		return this.canReceiveChangedMessage;
	}
	
	public void updateCanReceiveLocaleChangedMessage(){
		this.canReceiveChangedMessage = true;
	}
	
	public void setChips(int chips){
		this.chips = chips;
		
		if(this.chips < 0){
			this.chips = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
	}
	
	public void addChips(int chips){
		this.chips = this.chips + chips;
		
		if(this.chips < 0){
			this.chips = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
		
		p.sendMessage(APIMeta.CHAT_PREFIX + getTranslatedMessage("chips.gained", new LangVariable[]{new LangVariable("%gainedChips%", chips + ""), new LangVariable("%newChips%", getChips() + "")}));
	}
	
	public void reduceChips(int chips){
		this.chips = this.chips - chips;
		
		if(this.chips < 0){
			this.chips = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
	}
	
	public int getCoupons(){
		return this.coupons;
	}
	
	public void setCoupons(int coupons){
		this.coupons = coupons;
		
		if(this.coupons < 0){
			this.coupons = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
	}
	
	public void addCoupons(int coupons){
		this.coupons = this.coupons + coupons;
		
		if(this.coupons < 0){
			this.coupons = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
		
		p.sendMessage(APIMeta.CHAT_PREFIX + getTranslatedMessage("coupons.gained", new LangVariable[]{new LangVariable("%gainedCoupons%", coupons + ""), new LangVariable("%newCoupons%", getCoupons() + "")}));
	}
	
	public void reduceCoupons(int coupons){
		this.coupons = this.coupons - coupons;
		
		if(this.coupons < 0){
			this.coupons = 0;
		}
		
		if(getCurrentScoreboardType() == ScoreboardType.LOBBY){
			updateScoreboard(ScoreboardType.LOBBY);
		}
	}
	
	public ArrayList<String> getFriends(){
		return this.friends;
	}
	
	public String getCurrentServerName(){
		return this.server.toUpperCase();
	}
	
	public ScoreboardType getCurrentScoreboardType(){
		return this.sbtype;
	}
	
	public void setScoreboardType(ScoreboardType sbtype){
		this.sbtype = sbtype;
	}
	
	public void updateScoreboard(ScoreboardType sbtype){
		this.sbtype = sbtype;
		
		if(sbtype == ScoreboardType.LOBBY){
			if(getCurrentScoreboard() == null){
				this.pssb = new PlayerSpecifiedScoreboard(ScoreboardType.LOBBY, p);
			} else {
				getCurrentScoreboard().getScoreboard().getObjective(DisplaySlot.SIDEBAR).getScore("�6Coins:").setScore(getCoins());
				getCurrentScoreboard().getScoreboard().getObjective(DisplaySlot.SIDEBAR).getScore("�3Chips:").setScore(getChips());
				//getCurrentScoreboard().getScoreboard().getObjective(DisplaySlot.SIDEBAR).getScore("�5Coupons:").setScore(getCoupons());
			}
		}
	}
	
	public PlayerSpecifiedScoreboard getCurrentScoreboard(){
		return this.pssb;
	}
	
	public void updateDisplayName(){
		Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
			public void run(){
				if(Nicks.isNicked(p.getUniqueId())){
					p.setDisplayName(Rank.USER.getColor() + Nicks.getNick(p.getUniqueId()));
					if(ChestAPI.CHANGE_TABLIST_NAME){
						if(p.getDisplayName().length() > 16){
							p.setPlayerListName(p.getDisplayName().substring(0, 15));
						} else {
							p.setPlayerListName(p.getDisplayName());
						}
					}
					
					//ChestAPI.USER.addEntry(Nicks.getNick(p.getUniqueId()));
					
					//for(Player all : Bukkit.getOnlinePlayers()){
						//all.setScoreboard(ChestAPI.USER.getScoreboard());
					//}
				} else {
					p.setDisplayName(getRank().getColor() + p.getName());
					if(ChestAPI.CHANGE_TABLIST_NAME){
						if(p.getDisplayName().length() > 16){
							p.setPlayerListName(p.getDisplayName().substring(0, 15));
						} else {
							p.setPlayerListName(p.getDisplayName());
						}
					}
					
					/*for(Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()){
						if(team.hasPlayer(p)){
							team.removePlayer(p);
						}
					}*/
					
					/*if(getRank() == Rank.USER){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.USER.getDisplayName() + " " + p.getName());
						ChestAPI.USER.addPlayer(p);
					} else if(getRank() == Rank.PREMIUM || getRank() == Rank.LIFETIME_PREMIUM){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.PREMIUM.getDisplayName() + " " + p.getName());
						ChestAPI.PREMIUM.addPlayer(p);
					} else if(getRank() == Rank.VIP){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.VIP.getDisplayName() + " " + p.getName());
						ChestAPI.VIP.addPlayer(p);
					} else if(getRank() == Rank.YT){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.YT.getDisplayName() + " " + p.getName());
						ChestAPI.YT.addPlayer(p);
					} else if(getRank() == Rank.BUILD){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.BUILD.getDisplayName() + " " + p.getName());
						ChestAPI.BUILD.addPlayer(p);
					} else if(getRank() == Rank.TEAM){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.TEAM.getDisplayName() + " " + p.getName());
						ChestAPI.TEAM.addPlayer(p);
					} else if(getRank() == Rank.MOD){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.MOD.getDisplayName() + " " + p.getName());
						ChestAPI.MOD.addPlayer(p);
					} else if(getRank() == Rank.ADMIN){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.ADMIN.getDisplayName() + " " + p.getName());
						ChestAPI.ADMIN.addPlayer(p);
					} else if(getRank() == Rank.OWNER){
						//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "scoreboard teams join " + ChestAPI.OWNER.getDisplayName() + " " + p.getName());
						ChestAPI.OWNER.addPlayer(p);
					}
					
					for(Player all : Bukkit.getOnlinePlayers()){
						all.setScoreboard(ChestAPI.USER.getScoreboard());
					}*/
				}
			}
		}, 1*20);
	}
	
	public boolean hasPermission(Rank minRank){
		if(this.rank.getId() < minRank.getId()){
			return false;
		} else {
			return true;
		}
	}
	
	public boolean hasAchieved(int aid){
		if(AchievementUtil.getAchievers(aid).contains(getInternalId())){
			return true;
		} else {
			return false;
		}
	}
	
	public void achieve(int aid){
		Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
			public void run(){
				String pidstring = getInternalId() + "";
				
				if(!hasAchieved(aid)){
					String achievers = "";
					
					for(Integer i : AchievementUtil.getAchievers(aid)){
						achievers = achievers + i + ",";
					}
					
					String achieversNew = achievers + pidstring + ",";
					
					try {
						HandlerStorage.getHandler("main").execute("UPDATE `achievements` SET `achieved_by` = '" + achieversNew + "' WHERE id=" + aid + "");
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					p.playSound(p.getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
					
					addCoins(50);
					
					p.sendMessage("�a******* �k<({�r�a" + getTranslatedMessage("achieved.headline", null) + "�k})> �r�a*******");
					p.sendMessage("");
					p.sendMessage("    �e�l" + getTranslatedMessage("achievement.name." + aid, null));
					p.sendMessage("       �7" + getTranslatedMessage("achievement.description." + aid, null));
					p.sendMessage("");
					p.sendMessage("�a******* �k<({�r�a" + getTranslatedMessage("achieved.headline", null) + "�k})> �r�a*******");
					
					addGlobalPoints(50);
				}
			}
		}, 1*20);
	}
	
	public Translation getTranslation(){
		if(p.spigot().getLocale().equalsIgnoreCase("de_DE")){
			return LangUtil.getTranslations("DE");
		} else if(p.spigot().getLocale().equalsIgnoreCase("da_DK")){
			return LangUtil.getTranslations("DEN");
		} else {
			return LangUtil.getTranslations("EN");
		}
	}
	
	public void sendTranslatedMessage(String phraseID, LangVariable[] variables){
		p.sendMessage(getTranslatedMessage(phraseID, variables));
	}
	
	public String getTranslatedMessage(String phraseID, LangVariable[] variables){
		String r = "";
		
		r = ChatColor.translateAlternateColorCodes('&', getTranslation().getTranslation(phraseID));
		
		if(variables != null){
			for(LangVariable var : variables){
				r = r.replace(var.getVariableId(), var.getVariableValue());
			}
		}
			
		return r;
	}
	
	public void showMinecraftCredits(){
		CraftPlayer craft = (CraftPlayer)p;
        EntityPlayer nms = craft.getHandle();
        
        nms.viewingCredits = true;
        nms.playerConnection.sendPacket(new PacketPlayOutGameStateChange(4, 0.0F));
	}
	
	public void saveData(){
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `users` SET `uuid` = '" + UUIDFetcher.getUUID(p.getName()) + "', `name` = '" + p.getName() + "', `coins` = " + coins + ", `chips` = " + chips + ", `coupons` = " + coupons + ", global_points = " + globalPoints + ", setting_serverLocation = " + APIMeta.convertBooleanToInteger(optionServerLocation) + ", setting_showStats = " + APIMeta.convertBooleanToInteger(optionShowStats) + ", setting_autoNick = " + APIMeta.convertBooleanToInteger(optionAutoNick) + ", setting_allowFriendRequests = " + APIMeta.convertBooleanToInteger(optionFriendRequests) + ", setting_showPlayers = " + APIMeta.convertBooleanToInteger(optionShowPlayers) + ", setting_keepSpawn = " + APIMeta.convertBooleanToInteger(optionKeepSpawn) + ", setting_privateMsg = " + APIMeta.convertBooleanToInteger(optionPrivateMsg) + ", `locale` = '" + convertCurrentLocaleString(getBukkitPlayer().getName()) + "' WHERE uuid='" + UUIDFetcher.getUUID(p.getName()) + "'");
			for(int i : getUnlockedRewards()){
				HandlerStorage.getHandler("main").execute("UPDATE `rewards` SET `who_gained` = '" + Reward.getReward(i).getWhoGainedString_Live() + getInternalId() + "," + "' WHERE id = " + id);
			}
			updateDisplayName();
			syncRewards();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void connect(String servername){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(stream);
		
		try {
			out.writeUTF("Connect");
			out.writeUTF(servername);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		p.sendPluginMessage(ChestAPI.getInstance(), "BungeeCord", stream.toByteArray());
	}
	
	public void connectToLobby(){
		p.sendMessage(APIMeta.CHAT_PREFIX + getTranslatedMessage("lobby.connect", null));
		connect("LOBBY");
	}
	
	public boolean getShowServerLocation(){
		return optionServerLocation;
	}
	
	public void setShowServerLoc(boolean b){
		optionServerLocation = b;
	}
	
	public boolean getShowStats(){
		return optionShowStats;
	}
	
	public void setShowStats(boolean b){
		optionShowStats = b;
	}
	
	public boolean getAutoNick(){
		return optionAutoNick;
	}
	
	public void setAutoNick(boolean b){
		optionAutoNick = b;
	}
	
	public boolean getAllowFriendRequests(){
		return optionFriendRequests;
	}
	
	public void setAllowFriendRequests(boolean b){
		optionFriendRequests = b;
	}
	
	public boolean getShowPlayers(){
		return optionShowPlayers;
	}
	
	public void setShowPlayers(boolean b){
		optionShowPlayers = b;
	}
	
	public boolean getKeepSpawn(){
		return optionKeepSpawn;
	}
	
	public void setKeepSpawn(boolean b){
		optionKeepSpawn = b;
	}
	
	public boolean getPrivateMsg(){
		return optionPrivateMsg;
	}
	
	public void setPrivateMsg(boolean b){
		optionPrivateMsg = b;
	}
	
}
