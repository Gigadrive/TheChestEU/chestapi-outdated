package eu.thechest.api.user;

import java.util.HashMap;

import org.bukkit.entity.Player;

public class UserStorage {

	public static HashMap<Player, ChestUser> STORAGE = new HashMap<Player, ChestUser>();
	public static void register(Player p){
		if(!STORAGE.containsKey(p)){
			STORAGE.put(p, new ChestUser(p));
		} else {
			STORAGE.remove(p);
			STORAGE.put(p, new ChestUser(p));
		}
	}
	
	public static ChestUser getUserByPlayer(Player p){
		if(!STORAGE.containsKey(p)){
			register(p);
		}
		
		return STORAGE.get(p);
	}
	
}
