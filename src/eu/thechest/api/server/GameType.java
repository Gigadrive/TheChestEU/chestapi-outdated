package eu.thechest.api.server;

public enum GameType {

	LOBBY(),
	SURVIVAL_GAMES(),
	INFECTION_WARS(),
	DEATHMATCH(),
	SOUPPVP(),
	WRATH_OF_DUNGEONS(),
	OBSIDIAN_DEFENDERS();
	
}
