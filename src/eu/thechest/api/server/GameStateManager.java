package eu.thechest.api.server;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;

import eu.thechest.api.ChestAPI;

public class GameStateManager {

	private static GameState gamestate;
	
	public static void setGameState(GameState gs){
		gamestate = gs;
		((CraftServer)Bukkit.getServer()).getServer().setMotd(gs.getDisplay());
		
		if(gs == GameState.LOBBY){
			ChestAPI.ALLOW_VIP_JOIN = true;
		} else {
			ChestAPI.ALLOW_VIP_JOIN = false;
		}
	}
	
	public static GameState getGameState(){
		return gamestate;
	}
	
}
