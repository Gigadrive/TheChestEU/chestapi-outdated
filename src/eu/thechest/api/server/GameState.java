package eu.thechest.api.server;

public enum GameState {

	UNDEFINED("&8&lPreparing.."),
	LOBBY("&a&lLobby"),
	JOINABLE("&a&lOnline"),
	INGAME("&4&lIngame"),
	ENDING("&c&lEnding"),
	WARMUP("&7&lWarmup");
	
	private String display;
	
	GameState(String display){
		this.display = display;
	}
	
	public String getDisplay(){
		return this.display;
	}
}
