package eu.thechest.api;

import java.util.Random;

public class APIMeta {

	public static final String CHAT_PREFIX = "�6[TheChestEU] �a";
	public static final String CHAT_PREFIX_ERROR = CHAT_PREFIX + "�c";
	public static final String NOPERM = CHAT_PREFIX_ERROR + "Du hast keine Rechte f�r diese Aktion.";
	public static final String NOPLAYER = CHAT_PREFIX_ERROR + "Du musst ein Spieler sein.";
	public static String NOTONLINE(String name){
		return CHAT_PREFIX_ERROR + "Der Spieler �e" + name + " �cist nicht online."; 
	}
	
	public static Integer convertBooleanToInteger(boolean b){
		if(b){
			return 1;
		} else {
			return 0;
		}
	}
	
	public static boolean convertIntegerToBoolean(Integer i){
		if(i == 1){
			return true;
		} else {
			return false;
		}
	}
	
	public static int randomInteger(int min, int max){
		Random rdm = new Random();
		int rdmNm = rdm.nextInt((max - min) + 1) + min;
		
		return rdmNm;
	}
	
	public static double randomDouble(double min, double max){
		Random r = new Random();
		double randomValue = min + (max - min) * r.nextDouble();
		
		return randomValue;
	}
	
}
