package eu.thechest.api.cmd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.QueryResult;
import de.inventivegames.nickname.Nicks;
import eu.thechest.api.APIMeta;
import eu.thechest.api.ChestAPI;
import eu.thechest.api.NickUtil;
import eu.thechest.api.lang.LangVariable;
import eu.thechest.api.user.ChestUser;
import eu.thechest.api.user.OfflinePlayerUtil;
import eu.thechest.api.user.Rank;
import eu.thechest.api.user.Reward;
import eu.thechest.api.user.UUIDFetcher;
import eu.thechest.api.user.UserStorage;

public class MainExecutor implements CommandExecutor {

	public MainExecutor(){
		
	}
	
	public static ArrayList<String> VANISHED_PLAYERS = new ArrayList<String>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("money")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(label.equalsIgnoreCase("coins")){
					p.sendMessage(APIMeta.CHAT_PREFIX + "�6Coins �c" + u.getCoins());
				} else if(label.equalsIgnoreCase("chips")){
					p.sendMessage(APIMeta.CHAT_PREFIX + "�3Chips �c" + u.getChips());
				} else if(label.equalsIgnoreCase("coupons")){
					p.sendMessage(APIMeta.CHAT_PREFIX + "�5Coupons �c" + u.getCoupons());
				} else {
					p.sendMessage(APIMeta.CHAT_PREFIX + "�6Coins �c" + u.getCoins());
					p.sendMessage(APIMeta.CHAT_PREFIX + "�3Chips �c" + u.getChips());
					p.sendMessage(APIMeta.CHAT_PREFIX + "�5Coupons �c" + u.getCoupons());
				}
			} else {
				sender.sendMessage(APIMeta.NOPLAYER);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("reward")){
			if(!(sender instanceof Player)){
				if(args.length == 0){
					sender.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <Player> <Reward-ID>");
				} else if(args.length == 2){
					try {
						String playerName = args[0];
						Integer rewardId = Integer.parseInt(args[1]);
						
						if(Reward.getReward(rewardId).existanceString() != null){
							if(Bukkit.getPlayer(playerName) != null){
								Reward.getReward(rewardId).giveTo(Bukkit.getPlayer(playerName));
								sender.sendMessage(APIMeta.CHAT_PREFIX + "Reward sent.");
							} else {
								sender.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Player is offline.");
							}
						} else {
							sender.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "This reward does not exist.");
						}
					} catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("nicklist")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(u.hasPermission(Rank.MOD)){
					HashMap<Player, String> nicks = new HashMap<Player, String>();
					
					for(Player all : Bukkit.getOnlinePlayers()){
						if(Nicks.isNicked(all.getUniqueId())){
							nicks.put(all, Nicks.getNick(all.getUniqueId()));
						}
					}
					
					if(nicks.size() == 0){
						p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.list.nonicks", null));
					} else {
						for(Player all : nicks.keySet()){
							p.sendMessage(APIMeta.CHAT_PREFIX + UserStorage.getUserByPlayer(all).getRank().getColor() + all.getName() + " �e-> �8" + nicks.get(all));
						}
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("nick")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(u.hasPermission(Rank.VIP)){
					if(ChestAPI.ALLOW_NICK == true){
						if(args.length == 0){
							p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <Name|random|off>");
						} else {
							if(NickUtil.NICK_DELAY_STORAGE.contains(p.getName())){
								p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.wait", null));
							} else {
								if(args[0].equalsIgnoreCase("random")){
									if(!Nicks.isNicked(p.getUniqueId())){
										Collections.shuffle(NickUtil.RANDOM_NICKS);
										String nick = NickUtil.RANDOM_NICKS.get(0);
										if(Bukkit.getPlayer(nick) == null && OfflinePlayerUtil.getRank(nick) == Rank.USER){
											Nicks.setNick(p.getUniqueId(), nick);
											Nicks.setSkin(p.getUniqueId(), nick);
											ChestAPI.createNickLog(p.getName(), nick);
											
											Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
												public void run(){
													p.sendMessage(APIMeta.CHAT_PREFIX + u.getTranslatedMessage("nick.nickedTo", new LangVariable[]{new LangVariable("%nick%", nick)}));
												}
											}, 2*20);
											
											Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
												public void run(){
													u.updateDisplayName();
												}
											}, 2*20);
											
											Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
												public void run(){
													u.updateDisplayName();
												}
											}, 4*20);
											
											Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
												public void run(){
													u.updateDisplayName();
												}
											}, 6*20);
											
											NickUtil.NICK_DELAY_STORAGE.add(p.getName());
											Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
												public void run(){
													NickUtil.NICK_DELAY_STORAGE.remove(p.getName());
												}
											}, 11*20);
										} else {
											p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.cannotNick", null));
										}
									} else {
										p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.alreadyNicked", null));
									}
								} else if(args[0].equalsIgnoreCase("off") || args[0].equalsIgnoreCase(p.getName())){
									if(Nicks.isNicked(p.getUniqueId())){
										Nicks.removeNick(p.getUniqueId());
										Nicks.removeSkin(p.getUniqueId());
										p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.nickCleared", null));
										Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
											public void run(){
												u.updateDisplayName();
											}
										}, 2*20);
										
										Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
											public void run(){
												u.updateDisplayName();
											}
										}, 4*20);
										
										Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
											public void run(){
												u.updateDisplayName();
											}
										}, 6*20);
										
										NickUtil.NICK_DELAY_STORAGE.add(p.getName());
										Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
											public void run(){
												NickUtil.NICK_DELAY_STORAGE.remove(p.getName());
											}
										}, 11*20);
									} else {
										p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.notNicked", null));
									}
								} else {
									if(!Nicks.isNicked(p.getUniqueId())){
										String nick = args[0];
										if(nick.length() <= 16){
											if(Bukkit.getPlayer(nick) == null && OfflinePlayerUtil.getRank(nick) == Rank.USER){
												Nicks.setNick(p.getUniqueId(), nick);
												Nicks.setSkin(p.getUniqueId(), nick);
												ChestAPI.createNickLog(p.getName(), nick);
												
												p.sendMessage(APIMeta.CHAT_PREFIX + u.getTranslatedMessage("nick.nickedTo", new LangVariable[]{new LangVariable("%nick%", nick)}));
												Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
													public void run(){
														u.updateDisplayName();
													}
												}, 2*20);
												
												Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
													public void run(){
														u.updateDisplayName();
													}
												}, 4*20);
												
												Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
													public void run(){
														u.updateDisplayName();
													}
												}, 6*20);
												
												NickUtil.NICK_DELAY_STORAGE.add(p.getName());
												Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
													public void run(){
														NickUtil.NICK_DELAY_STORAGE.remove(p.getName());
													}
												}, 11*20);
											} else {
												p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.cannotNick", null));
											}
										} else {
											p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.tooLong", null));
										}
									} else {
										p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.alreadyNicked", null));
									}
								}
							}
						}
					} else {
						p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + u.getTranslatedMessage("nick.disabled", null));
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("setlocation")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(u.hasPermission(Rank.ADMIN)){
					if(args.length == 0){
						p.sendMessage("�c/" + label + " <Name>");
					} else {
						p.sendMessage("�eSaving location..");
						try {
							ChestAPI.saveLocationToConfig(p.getLocation(), args[0]);
							p.sendMessage("�aSaved location to");
							p.sendMessage("�aW: " + p.getLocation().getWorld().getName());
							p.sendMessage("�aX: " + p.getLocation().getX());
							p.sendMessage("�aY: " + p.getLocation().getY());
							p.sendMessage("�aZ: " + p.getLocation().getZ());
							p.sendMessage("�aYAW: " + p.getLocation().getYaw());
							p.sendMessage("�aPITCH: " + p.getLocation().getPitch());
						} catch(Exception e){
							e.printStackTrace();
							p.sendMessage("�cFailed.");
						}
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("goto")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(u.hasPermission(Rank.ADMIN)){
					if(args.length == 0){
						p.sendMessage("�c/" + label + " <Name>");
					} else {
						if(ChestAPI.getLocationFromConfig(args[0]) != null){
							p.teleport(ChestAPI.getLocationFromConfig(args[0]));
							p.sendMessage("�eTeleported.");
						} else {
							p.sendMessage("�cUnknown ID");
						}
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("clearchat")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(u.hasPermission(Rank.STAFF)){
					System.out.println("[INFO] " + p.getName() + " cleared the chat.");
					
					for(Player all : Bukkit.getOnlinePlayers()){
						if(!UserStorage.getUserByPlayer(all).hasPermission(Rank.STAFF)){
							for (int i = 0; i < 150; i++) {
								all.sendMessage(" ");
							}
							
							all.sendMessage("   �3�k...�c " + UserStorage.getUserByPlayer(all).getTranslatedMessage("api.chatcleared", null) + " �3�k...");
							all.sendMessage(" ");
						} else {
							all.sendMessage(" ");
							all.sendMessage("   �3�k...�c " + UserStorage.getUserByPlayer(all).getTranslatedMessage("api.chatcleared", null) + " �3�k...");
							all.sendMessage(" ");
						}
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("skull")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(args.length == 0){
					p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <Name>");
				} else {
					String pn = args[0];
					
					ItemStack head = new ItemStack(Material.SKULL_ITEM);
					head.setDurability((short)3);
					SkullMeta headM = (SkullMeta)head.getItemMeta();
					headM.setOwner(pn);
					head.setItemMeta(headM);
					
					p.getInventory().addItem(head);
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("vanish")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				if(cmd.getName().equalsIgnoreCase("vanish")){
					return false;
				}
				if(u.hasPermission(Rank.STAFF)){
					if(VANISHED_PLAYERS.contains(p.getName())){
						for(Player all : Bukkit.getOnlinePlayers()){
							all.showPlayer(p);
						}
						
						VANISHED_PLAYERS.remove(p.getName());
						p.sendMessage(APIMeta.CHAT_PREFIX + "Du bist nun wieder sichtbar.");
						p.removePotionEffect(PotionEffectType.INVISIBILITY);
					} else {
						for(Player all : Bukkit.getOnlinePlayers()){
							if(!UserStorage.getUserByPlayer(all).hasPermission(Rank.STAFF)){
								all.hidePlayer(p);
							}
						}
						
						VANISHED_PLAYERS.add(p.getName());
						p.sendMessage(APIMeta.CHAT_PREFIX + "Du bist nun unsichtbar");
						p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, false));
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("insertpaymentdata")){
			if(sender instanceof ConsoleCommandSender){
				if(args.length == 9){
					String name = args[0];
					String uuid = UUIDFetcher.getUUID(name);
					String transid = args[1];
					String price = args[2];
					String currency = args[3];
					String time = args[4];
					String date = args[5];
					String ip = args[6];
					String packageId = args[7];
					String packageExpiry = args[8];
					
					String query = "INSERT INTO `transactions` (`name`, `uuid`, `transid`, `price`, `currency`, `time`, `date`, `ip`, `packageId`, `packageExpiry`)VALUES ('" + name + "', '" + uuid + "', '" + transid + "', '" + price + "', '" + currency + "', '" + time + "', '" + date + "', '" + ip + "', " + packageId + ", '" + packageExpiry + "');";
					System.out.println("");
					System.out.println("");
					System.out.println(query);
					System.out.println("");
					System.out.println("");
					
					try {
						HandlerStorage.getHandler("main").execute(query);
					} catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("premium")){
			if(sender instanceof ConsoleCommandSender){
				if(args.length == 1){
					String name = args[0];
					
					try {
						if(UUIDFetcher.getUUID(name) != null && UUIDFetcher.getUUID(name) != ""){
							String uuid = UUIDFetcher.getUUID(name);
							HandlerStorage.getHandler("main").execute("UPDATE users SET rank = 1 WHERE uuid='" + uuid + "'");
						}
					} catch(Exception e){}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("addchips")){
			if(sender instanceof ConsoleCommandSender){
				if(args.length == 2){
					String name = args[0];
					
					if(Bukkit.getPlayer(name) != null){
						UserStorage.getUserByPlayer(Bukkit.getPlayer(name)).addChips(Integer.parseInt(args[1]));
					} else {
						System.err.println("[ChestAPI BuyCraft Implementation] COULD NOT GIVE CHIPS (" + args[1] + ") TO PLAYER " + name);
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("addcoupons")){
			if(sender instanceof ConsoleCommandSender){
				if(args.length == 2){
					String name = args[0];
					
					if(Bukkit.getPlayer(name) != null){
						UserStorage.getUserByPlayer(Bukkit.getPlayer(name)).addCoupons(Integer.parseInt(args[1]));
					} else {
						System.err.println("[ChestAPI BuyCraft Implementation] COULD NOT GIVE COUPONS (" + args[1] + ") TO PLAYER " + name);
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("unpremium")){
			if(sender instanceof ConsoleCommandSender){
				if(args.length == 1){
					String name = args[0];
					
					try {
						if(UUIDFetcher.getUUID(name) != null && UUIDFetcher.getUUID(name) != ""){
							String uuid = UUIDFetcher.getUUID(name);
							HandlerStorage.getHandler("main").execute("UPDATE users SET rank = 0 WHERE uuid='" + uuid + "'");
						}
					} catch(Exception e){}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("lifetimepremium")){
			if(sender instanceof ConsoleCommandSender){
				if(args.length == 1){
					String name = args[0];
					
					try {
						if(UUIDFetcher.getUUID(name) != null && UUIDFetcher.getUUID(name) != ""){
							String uuid = UUIDFetcher.getUUID(name);
							HandlerStorage.getHandler("main").execute("UPDATE users SET rank = 2 WHERE uuid='" + uuid + "'");
						}
					} catch(Exception e){}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("redeemkey")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(u.hasPermission(Rank.ADMIN)){
					if(args.length == 0){
						p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <Spieler>");
					} else {
						String name = args[0];
						
						if(UUIDFetcher.getUUID(name) != null && UUIDFetcher.getUUID(name) != ""){
							try {
								QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM whitelist WHERE uuid='" + UUIDFetcher.getUUID(name) + "'");
								
								qr.rs.last();
								if(qr.rs.getRow() == 0){
									HandlerStorage.getHandler("main").execute("INSERT INTO `whitelist` (`uuid`) VALUES('" + UUIDFetcher.getUUID(name) + "');");
									p.sendMessage(APIMeta.CHAT_PREFIX + "�e" + name + " �aist nun auf der Whitelist.");
								} else {
									p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Dieser Spieler hat bereits einen Key eingel�st.");
								}
							} catch(Exception e){
								e.printStackTrace();
								p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Ein Fehler ist aufgetreten.");
							}
						} else {
							p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Dieser Minecraft-Account existiert nicht!");
						}
					}
				}
			} else {
				CommandSender p = sender;
				
				if(args.length == 0){
					p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <Spieler>");
				} else {
					String name = args[0];
					
					if(UUIDFetcher.getUUID(name) != null && UUIDFetcher.getUUID(name) != ""){
						try {
							QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM whitelist WHERE uuid='" + UUIDFetcher.getUUID(name) + "'");
							
							qr.rs.last();
							if(qr.rs.getRow() == 0){
								HandlerStorage.getHandler("main").execute("INSERT INTO `whitelist` (`uuid`) VALUES('" + UUIDFetcher.getUUID(name) + "');");
								p.sendMessage(APIMeta.CHAT_PREFIX + "�e" + name + " �aist nun auf der Whitelist.");
							} else {
								p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Dieser Spieler hat bereits einen Key eingel�st.");
							}
						} catch(Exception e){
							e.printStackTrace();
							p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Ein Fehler ist aufgetreten.");
						}
					} else {
						p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Dieser Minecraft-Account existiert nicht!");
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("blockloc")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(u.hasPermission(Rank.ADMIN)){
					ItemStack stack = new ItemStack(Material.BLAZE_ROD);
					ItemMeta stackM = stack.getItemMeta();
					stackM.setDisplayName("�cBlockLoc");
					stack.setItemMeta(stackM);
					p.getInventory().addItem(stack);
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("fixplayer")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(args.length == 0){
					p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <Spieler-Name|all>");
				} else {
					if(args[0].equalsIgnoreCase("all")){
						for(Player all : Bukkit.getOnlinePlayers()){
							if(all != p){
								if(!VANISHED_PLAYERS.contains(all.getName())){
									p.hidePlayer(all);
								}
							}
						}
						
						Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
							public void run(){
								for(Player all : Bukkit.getOnlinePlayers()){
									if(all != p){
										if(!VANISHED_PLAYERS.contains(all.getName())){
											p.showPlayer(all);
										}
									}
								}
							}
						});
						
						p.sendMessage(APIMeta.CHAT_PREFIX + "Alle Spieler wurden gefixt!");
					} else if(args[0].equalsIgnoreCase(p.getName())){
						p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Du kannst dich nicht selbst fixen!");
					} else {
						if(Bukkit.getPlayer(args[0]) != null){
							if(VANISHED_PLAYERS.contains(args[0])){
								p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "Dieser Spieler kann nicht gefixt werden.");
							} else {
								p.hidePlayer(Bukkit.getPlayer(args[0]));
								
								Bukkit.getScheduler().scheduleSyncDelayedTask(ChestAPI.getInstance(), new Runnable(){
									public void run(){
										p.showPlayer(Bukkit.getPlayer(args[0]));
									}
								});
							}
						} else {
							p.sendMessage(APIMeta.NOTONLINE(args[0]));
						}
					}
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("help")){
			if(args.length == 0){
				sender.sendMessage("�8========== �e/help �61/2 �8==========");
				sender.sendMessage("�c/help �7Zeigt eine Befehlsliste");
				sender.sendMessage("�c/party �7Verwalte deine Party");
				sender.sendMessage("�c/money �7Zeigt deinen Kontostand an");
				sender.sendMessage("�c/ping �7Zeigt deine Serververbindung in ms");
				sender.sendMessage("�c/report �7Melde einen Spieler.");
				sender.sendMessage("�c/ticket �7Melde einen Fehler.");
				sender.sendMessage("�8========== �e/help �61/2 �8==========");
			} else {
				if(args[0].equalsIgnoreCase("1")){
					sender.sendMessage("�8========== �e/help �61/2 �8==========");
					sender.sendMessage("�c/help �7Zeigt eine Befehlsliste");
					sender.sendMessage("�c/party �7Verwalte deine Party");
					sender.sendMessage("�c/money �7Zeigt deinen Kontostand an");
					sender.sendMessage("�c/ping �7Zeigt deine Serververbindung in ms");
					sender.sendMessage("�c/report �7Melde einen Spieler.");
					sender.sendMessage("�c/ticket �7Melde einen Fehler.");
					sender.sendMessage("�8========== �e/help �61/2 �8==========");
				} else if(args[0].equalsIgnoreCase("2")){
					sender.sendMessage("�8========== �e/help �62/2 �8==========");
					sender.sendMessage("�c/list �7Zeigt die Online-Spieler an");
					sender.sendMessage("�c/hub �7Kehre zur Lobby zur�ck");
					sender.sendMessage("�c/friend �7Verwalte deine Freunde");
					sender.sendMessage("�c/msg �7Rede mit einem anderen Spieler (privat)");
					sender.sendMessage("�c/whereami �7Zeigt deinen aktuellen Server an");
					sender.sendMessage("�c/register �7Registriere dich f�r die Website.");
					sender.sendMessage("�8========== �e/help �62/2 �8==========");
				} else {
					sender.sendMessage("�cDiese Seite existiert nicht.");
				}
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("gamemode")){
			if(sender instanceof Player){
				Player p = (Player)sender;
				ChestUser u = UserStorage.getUserByPlayer(p);
				
				if(u.hasPermission(Rank.BUILD_TEAM)){
					if(args.length == 0){
						p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <survival|creative|adventure|spectator|s|c|a|sp|0|1|2|3> [Spieler]");
					} else if(args.length == 1){
						if(args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("s") || args[0].equalsIgnoreCase("0")){
							p.setGameMode(GameMode.SURVIVAL);
							p.sendMessage("�aNeuer Spielmodus: �e" + p.getGameMode().toString());
						} else if(args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("c") || args[0].equalsIgnoreCase("1")){
							p.setGameMode(GameMode.CREATIVE);
							p.sendMessage("�aNeuer Spielmodus: �e" + p.getGameMode().toString());
						} else if(args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("a") || args[0].equalsIgnoreCase("2")){
							p.setGameMode(GameMode.ADVENTURE);
							p.sendMessage("�aNeuer Spielmodus: �e" + p.getGameMode().toString());
						} else if(args[0].equalsIgnoreCase("spectator") || args[0].equalsIgnoreCase("sp") || args[0].equalsIgnoreCase("3")){
							p.setGameMode(GameMode.SPECTATOR);
							p.sendMessage("�aNeuer Spielmodus: �e" + p.getGameMode().toString());
							p.sendMessage("�cBitte benutze um unsichtbar zu bleiben �e/vanish");
						} else {
							p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <survival|creative|adventure|spectator|s|c|a|sp|0|1|2|3> [Spieler]");
						}
					} else if(args.length == 2){
						if(Bukkit.getPlayer(args[1]) != null){
							Player p2 = Bukkit.getPlayer(args[1]);
							
							if(args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("s") || args[0].equalsIgnoreCase("0")){
								p2.setGameMode(GameMode.SURVIVAL);
								p.sendMessage("�aNeuer Spielmodus von " + p2.getName() + ": �e" + p2.getGameMode().toString());
							} else if(args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("c") || args[0].equalsIgnoreCase("1")){
								p2.setGameMode(GameMode.CREATIVE);
								p.sendMessage("�aNeuer Spielmodus von " + p2.getName() + ": �e" + p2.getGameMode().toString());
							} else if(args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("a") || args[0].equalsIgnoreCase("2")){
								p2.setGameMode(GameMode.ADVENTURE);
								p.sendMessage("�aNeuer Spielmodus von " + p2.getName() + ": �e" + p2.getGameMode().toString());
							} else if(args[0].equalsIgnoreCase("spectator") || args[0].equalsIgnoreCase("sp") || args[0].equalsIgnoreCase("3")){
								p2.setGameMode(GameMode.SPECTATOR);
								p.sendMessage("�aNeuer Spielmodus von " + p2.getName() + ": �e" + p2.getGameMode().toString());
								p.sendMessage("�cBitte benutze um unsichtbar zu bleiben �e/vanish");
							} else {
								p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <survival|creative|adventure|spectator|s|c|a|sp|0|1|2|3> [Spieler]");
							}
						} else {
							p.sendMessage(APIMeta.NOTONLINE(args[1]));
						}
					} else {
						p.sendMessage(APIMeta.CHAT_PREFIX_ERROR + "/" + label + " <survival|creative|adventure|spectator|s|c|a|sp|0|1|2|3> [Spieler]");
					}
				} else {
					p.sendMessage(APIMeta.NOPERM);
				}
			} else {
				sender.sendMessage(APIMeta.NOPLAYER);
			}
		}
		
		return false;
	}
	
}
